<?php
require_once 'vendor/autoload.php';

use \Slim\Slim;

$app = new Slim(array(
  'debug' => true,
));

$app->get('/', function() use ($app) {
  echo 'hello, world';
});

$app->get('/hello/:name', function($name) use ($app) {
  echo 'hello, ' . $name;
});

$app->run();
